package edu.iasp.pruebas.web.componente01.utils;

import com.google.gson.Gson;
import edu.iasp.pruebas.web.componente01.exceptions.direcciones.CalleInvalidaException;
import edu.iasp.pruebas.web.componente01.exceptions.direcciones.DireccionInvalidaException;
import edu.iasp.pruebas.web.componente01.exceptions.direcciones.ViaPrimariaInvalidaException;
import edu.iasp.pruebas.web.componente01.exceptions.enums.CodigoMensajeErrorEnum;
import edu.iasp.pruebas.web.componente01.info.vo.DireccionForm;

import java.util.Objects;

public class DireccionUtils {

    private static final String SEPARADOR = " ";
    private static final String SEPARADOR_COMPLEMENTO = ", ";
    private static final String NRO = "No.";
    private static final String BIS = "BIS";

    private DireccionUtils() {

    }

    public static boolean isValid(DireccionForm direccionForm) throws DireccionInvalidaException {
        if (Objects.isNull(direccionForm.getCalle()) || direccionForm.getCalle().equals(SEPARADOR) || direccionForm.getCalle().isEmpty())
            throw new CalleInvalidaException(CodigoMensajeErrorEnum.CALLE_NO_SELECCIONADA);
        if (Objects.isNull(direccionForm.getNombreViaPrimaria()) || direccionForm.getNombreViaPrimaria().isEmpty())
            throw new ViaPrimariaInvalidaException(CodigoMensajeErrorEnum.VIA_PRINCIPAL_NO_DIGITADA);
        return true;

    }

    public static String toString(DireccionForm direccionForm) {
        StringBuilder direccion = new StringBuilder();
        if(Objects.nonNull(direccionForm)){
            direccion
                .append(appendViaPrimaria(direccionForm))
                .append(appendViaSecundaria(direccionForm))
                .append(appendViaComplementaria(direccionForm))
                .append(appendComplementos(direccionForm));
        }
        return direccion.toString();
    }

    public static String toJson(DireccionForm direccionForm){
        return new Gson().toJson(direccionForm);
    }

    public static DireccionForm jsonToDireccionForm(String direccionJson){
        return new Gson().fromJson(direccionJson, DireccionForm.class);
    }

    private static String appendViaPrimaria(DireccionForm direccionForm) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!StringUtils.isEmpty(direccionForm.getCalle())) {
            stringBuilder.append(direccionForm.getCalle());
        }
        if (!StringUtils.isEmpty(direccionForm.getNombreViaPrimaria())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getNombreViaPrimaria());
        }
        if (!StringUtils.isEmpty(direccionForm.getLetrasViaPrimaria1())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getLetrasViaPrimaria1());
        }
        if (direccionForm.isBisViaPrimaria()) {
            stringBuilder.append(SEPARADOR).append(BIS);
        }
        if (!StringUtils.isEmpty(direccionForm.getLetrasViaPrimaria2())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getLetrasViaPrimaria2());
        }
        if (!StringUtils.isEmpty(direccionForm.getCardinalViaPrimaria())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getCardinalViaPrimaria());
        }
        return stringBuilder.toString();
    }

    private static String appendViaSecundaria(DireccionForm direccionForm) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!StringUtils.isEmpty(direccionForm.getNumeroViaSecundaria())) {
            stringBuilder.append(SEPARADOR).append(NRO).append(SEPARADOR).append(direccionForm.getNumeroViaSecundaria());
        }
        if (!StringUtils.isEmpty(direccionForm.getLetrasViaSecundaria())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getLetrasViaSecundaria());
        }
        if (direccionForm.isBisViaSecundaria()) {
            stringBuilder.append(SEPARADOR).append(BIS);
        }
        return stringBuilder.toString();
    }

    private static String appendViaComplementaria(DireccionForm direccionForm) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!StringUtils.isEmpty(direccionForm.getNumeroViaComplementaria())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getNumeroViaComplementaria());
        }
        if (!StringUtils.isEmpty(direccionForm.getCardinalViaComplementaria())) {
            stringBuilder.append(SEPARADOR).append(direccionForm.getCardinalViaComplementaria());
        }
        return stringBuilder.toString();
    }

    private static String appendComplementos(DireccionForm direccionForm) {
        StringBuilder stringBuilder = new StringBuilder();
        direccionForm.getComplementos().forEach((k, v) ->
            stringBuilder.append(k).append(SEPARADOR).append(v).append(SEPARADOR)
        );
        if(stringBuilder.length() > 0){
            stringBuilder.insert(0, SEPARADOR_COMPLEMENTO);
        }
        return stringBuilder.toString();
    }
}
