package edu.iasp.pruebas.web.componente01.exceptions.direcciones;

import edu.iasp.pruebas.web.componente01.exceptions.enums.CodigoMensajeErrorEnum;

public class CalleInvalidaException extends DireccionInvalidaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6554015676470402849L;

	public CalleInvalidaException(CodigoMensajeErrorEnum codigo) {
		super(codigo);
	}

	
}
