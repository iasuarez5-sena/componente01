package edu.iasp.pruebas.web.componente01.exceptions.direcciones;


import edu.iasp.pruebas.web.componente01.exceptions.enums.CodigoMensajeErrorEnum;

public class ViaPrimariaInvalidaException extends DireccionInvalidaException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5216905211339790852L;

	public ViaPrimariaInvalidaException(CodigoMensajeErrorEnum codigo) {
		super(codigo);
	}

}
