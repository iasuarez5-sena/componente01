package edu.iasp.pruebas.web.componente01.component;

import edu.iasp.pruebas.web.componente01.exceptions.direcciones.DireccionInvalidaException;
import edu.iasp.pruebas.web.componente01.info.enums.DireccionCalleEnum;
import edu.iasp.pruebas.web.componente01.info.enums.DireccionCardinalEnum;
import edu.iasp.pruebas.web.componente01.info.enums.DireccionComplementoEnum;
import edu.iasp.pruebas.web.componente01.info.vo.DireccionForm;
import edu.iasp.pruebas.web.componente01.utils.DireccionUtils;
import edu.iasp.pruebas.web.componente01.utils.ViewUtils;

import javax.el.ValueExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.*;

@FacesComponent("com.conexia.one.wap.components.DireccionComponent")
public class DireccionComponent extends UINamingContainer {

    private static final String STRING_EMPTY = "";
    private static final int INDEX_STATE_SUPER = 0;
    private static final int INDEX_STATE_DIRECCION_FORM = 1;
    private static final int INDEX_STATE_KEY_COMPLEMENTO = 2;
    private static final int INDEX_STATE_VALOR_COMPLEMENTO = 3;
    private static final String BASE_NAME_RESOURCE_COMPONENTS = "i18n.components.mensajes";
    private static final String DEFAULT_DIALOG_ID = "dlgEditarDireccion";
    private static final String DEFAULT_DIALOG_WV = "WV";

    private static List<DireccionCardinalEnum> selectCardinal = generarSelectCardinales();
    private static List<DireccionCalleEnum> selectCalles = generarSelectCalles();
    private static List<DireccionComplementoEnum> selectComplemento = generarSelectComplementos();
    private static ResourceBundle resourceBundle;

    enum PropertyKeys{
        value, valueAsJson, dialogId, dialogWV
    }

    private DireccionForm direccionForm;
    private String keyComplemento;
    private String valorComplemento;


    public void refrescarDireccion() {
        direccionForm.setDireccionCompleta(DireccionUtils.toString(direccionForm));
    }

    public void cargarDireccion(){
        try {
            direccionForm = DireccionUtils.jsonToDireccionForm(getValue());
            if(Objects.isNull(direccionForm)){
                direccionForm = new DireccionForm();
            }
        } catch (Exception e){
            direccionForm = new DireccionForm();
        }
    }

    public void agregarComplemento() {
        direccionForm.getComplementos().put(keyComplemento, valorComplemento);
        refrescarDireccion();
    }

    public void limpiarComplemento() {
        direccionForm.setComplementos(new HashMap<>());
        refrescarDireccion();
    }

    public void guardar() {
        try {
            DireccionUtils.isValid(this.direccionForm);
            ValueExpression valueExpression = getValueExpression(PropertyKeys.value.toString());
            if(Objects.nonNull(valueExpression)){
                valueExpression.setValue(getFacesContext().getELContext(),
                    isValueAsJson() ? DireccionUtils.toJson(direccionForm) : direccionForm.getDireccionCompleta());
            }
            ViewUtils.closeDialog(getDialogWV());
        } catch (DireccionInvalidaException e) {
            ViewUtils.addErrorMessage(getResourceBoundle().getString(e.getCodigo().getMensajeI18n()), STRING_EMPTY);
        }

    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        cargarDireccion();
        super.encodeBegin(context);
    }


    @Override
    public void encodeEnd(FacesContext context) throws IOException {
        cargarDireccion();
        super.encodeEnd(context);
    }

    @Override
    public Object saveState(FacesContext context) {
        return new Object[] {super.saveState(context), direccionForm, keyComplemento, valorComplemento};
    }

    @Override
    public void restoreState(FacesContext context, Object state) {
        Object[] estados = (Object[]) state;
        super.restoreState(context, estados[INDEX_STATE_SUPER]);
        direccionForm = (DireccionForm) estados[INDEX_STATE_DIRECCION_FORM];
        keyComplemento = (String) estados[INDEX_STATE_KEY_COMPLEMENTO];
        valorComplemento = (String) estados[INDEX_STATE_VALOR_COMPLEMENTO];
    }



    private static List<DireccionCardinalEnum> generarSelectCardinales() {
        return Arrays.asList(DireccionCardinalEnum.values());
    }

    private static List<DireccionCalleEnum> generarSelectCalles() {
        return Arrays.asList(DireccionCalleEnum.values());
    }

    private static List<DireccionComplementoEnum> generarSelectComplementos() {
        return Arrays.asList(DireccionComplementoEnum.values());
    }

    private static ResourceBundle getResourceBoundle() {
        if(Objects.isNull(resourceBundle)){
            resourceBundle = ResourceBundle.getBundle(BASE_NAME_RESOURCE_COMPONENTS, FacesContext.getCurrentInstance().getViewRoot().getLocale());
        }
        return resourceBundle;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters && Setters - Fields">

    public String getValorComplemento() {
        return valorComplemento;
    }

    public void setValorComplemento(String valorComplemento) {
        this.valorComplemento = valorComplemento;
    }

    public String getKeyComplemento() {
        return keyComplemento;
    }

    public void setKeyComplemento(String keyComplemento) {
        this.keyComplemento = keyComplemento;
    }

    public List<DireccionCardinalEnum> getSelectCardinal() {
        return selectCardinal;
    }

    public List<DireccionCalleEnum> getSelectCalles() {
        return selectCalles;
    }

    public List<DireccionComplementoEnum> getSelectComplemento() {
        return selectComplemento;
    }

    public DireccionForm getDireccionForm() {
        return direccionForm;
    }

    public void setDireccionForm(DireccionForm direccionForm) {
        this.direccionForm = direccionForm;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Getters && Setters - Facade">

    public String getCalle() {
        return direccionForm.getCalle();
    }

    public void setCalle(String calle) {
        direccionForm.setCalle(calle);
    }

    public String getNombreViaPrimaria() {
        return direccionForm.getNombreViaPrimaria();
    }

    public void setNombreViaPrimaria(String nombreViaPrimaria) {
        direccionForm.setNombreViaPrimaria(nombreViaPrimaria);
    }

    public String getLetrasViaPrimaria1() {
        return direccionForm.getLetrasViaPrimaria1();
    }

    public void setLetrasViaPrimaria1(String letrasViaPrimaria1) {
        direccionForm.setLetrasViaPrimaria1(letrasViaPrimaria1);
    }

    public boolean isBisViaPrimaria() {
        return direccionForm.isBisViaPrimaria();
    }

    public void setBisViaPrimaria(boolean bisViaPrimaria) {
        direccionForm.setBisViaPrimaria(bisViaPrimaria);
    }

    public String getLetrasViaPrimaria2() {
        return direccionForm.getLetrasViaPrimaria2();
    }

    public void setLetrasViaPrimaria2(String letrasViaPrimaria2) {
        direccionForm.setLetrasViaPrimaria2(letrasViaPrimaria2);
    }

    public String getCardinalViaPrimaria() {
        return direccionForm.getCardinalViaPrimaria();
    }

    public void setCardinalViaPrimaria(String cardinalViaPrimaria) {
        direccionForm.setCardinalViaPrimaria(cardinalViaPrimaria);
    }

    public String getNumeroViaSecundaria() {
        return direccionForm.getNumeroViaSecundaria();
    }

    public void setNumeroViaSecundaria(String numeroViaSecundaria) {
        direccionForm.setNumeroViaSecundaria(numeroViaSecundaria);
    }

    public String getLetrasViaSecundaria() {
        return direccionForm.getLetrasViaSecundaria();
    }

    public void setLetrasViaSecundaria(String letrasViaSecundaria) {
        direccionForm.setLetrasViaSecundaria(letrasViaSecundaria);
    }

    public boolean isBisViaSecundaria() {
        return direccionForm.isBisViaSecundaria();
    }

    public void setBisViaSecundaria(boolean bisViaSecundaria) { direccionForm.setBisViaSecundaria(bisViaSecundaria);
    }

    public String getNumeroViaComplementaria() {
        return direccionForm.getNumeroViaComplementaria();
    }

    public void setNumeroViaComplementaria(String numeroViaComplementaria) {
        direccionForm.setNumeroViaComplementaria(numeroViaComplementaria);
    }

    public String getCardinalViaComplementaria() {
        return direccionForm.getCardinalViaComplementaria();
    }

    public void setCardinalViaComplementaria(String cardinalViaComplementaria) {
        direccionForm.setCardinalViaComplementaria(cardinalViaComplementaria);
    }

    public String getDireccionCompleta() {
        return direccionForm.getDireccionCompleta();
    }

    public void setDireccionCompleta(String direccionCompleta) {
        direccionForm.setDireccionCompleta(direccionCompleta);
    }
    //</editor-fold >

    //<editor-fold defaultstate="collapsed" desc="Getters && Setters - Properties">
    public String getValue(){
        return (String) getStateHelper().eval(PropertyKeys.value);
    }

    public void setValue(String value){
        getStateHelper().put(PropertyKeys.value, value);
    }

    public boolean isValueAsJson(){
        return (boolean) getStateHelper().eval(PropertyKeys.valueAsJson, true);
    }

    public void setValueAsJson(boolean valueAsJson){
        getStateHelper().put(PropertyKeys.valueAsJson, valueAsJson);
    }

    public String getDialogId(){
        return (String) getStateHelper().eval(PropertyKeys.dialogId, DEFAULT_DIALOG_ID);
    }

    public void setDialogId(String dialogId){
        getStateHelper().put(PropertyKeys.dialogId, dialogId);
    }

    public String getDialogWV(){
        return (String) getStateHelper().eval(PropertyKeys.dialogWV, getDefaultDialogWV());
    }

    public void setDialogWV(String dialogWV){
        getStateHelper().put(PropertyKeys.dialogWV, dialogWV);
    }

    public String getDefaultDialogWV(){
        return getClientId() + ":" + getDialogId()  + DEFAULT_DIALOG_WV;
    }
    //</editor-fold >
}
