package edu.iasp.pruebas.web.componente01.exceptions.direcciones;


import edu.iasp.pruebas.web.componente01.exceptions.enums.CodigoMensajeErrorEnum;

public class ViaSecundariaInvalidaException extends DireccionInvalidaException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400712089451074279L;

	public ViaSecundariaInvalidaException(CodigoMensajeErrorEnum codigo) {
		super(codigo);
	}

}
