package edu.iasp.pruebas.web.componente01.info.enums;

import java.io.Serializable;

public enum DireccionCalleEnum implements Serializable {
    AC,
    AK,
    CL,
    KR,
    DG,
    TV;
}
