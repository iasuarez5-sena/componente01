package edu.iasp.pruebas.web.componente01.info.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DireccionForm implements Serializable {

    private String calle = "";
    private String nombreViaPrimaria = "";
    private String letrasViaPrimaria1 = "";
    private boolean bisViaPrimaria = false;
    private String letrasViaPrimaria2 = "";
    private String cardinalViaPrimaria = "";
    private String numeroViaSecundaria = "";
    private String letrasViaSecundaria = "";
    private boolean bisViaSecundaria = false;
    private String numeroViaComplementaria = "";
    private String cardinalViaComplementaria = "";
    private Map<String, String> complementos;
    private String direccionCompleta = "";

    public DireccionForm() {
        complementos = new HashMap<>();
    }

    //<editor-fold defaultstate="collapsed" desc="Getters && Setters">
    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNombreViaPrimaria() {
        return nombreViaPrimaria;
    }

    public void setNombreViaPrimaria(String nombreViaPrimaria) {
        this.nombreViaPrimaria = nombreViaPrimaria;
    }

    public String getLetrasViaPrimaria1() {
        return letrasViaPrimaria1;
    }

    public void setLetrasViaPrimaria1(String letrasViaPrimaria1) {
        this.letrasViaPrimaria1 = letrasViaPrimaria1;
    }

    public boolean isBisViaPrimaria() {
        return bisViaPrimaria;
    }

    public void setBisViaPrimaria(boolean bisViaPrimaria) {
        this.bisViaPrimaria = bisViaPrimaria;
    }

    public String getLetrasViaPrimaria2() {
        return letrasViaPrimaria2;
    }

    public void setLetrasViaPrimaria2(String letrasViaPrimaria2) {
        this.letrasViaPrimaria2 = letrasViaPrimaria2;
    }

    public String getCardinalViaPrimaria() {
        return cardinalViaPrimaria;
    }

    public void setCardinalViaPrimaria(String cardinalViaPrimaria) {
        this.cardinalViaPrimaria = cardinalViaPrimaria;
    }

    public String getNumeroViaSecundaria() {
        return numeroViaSecundaria;
    }

    public void setNumeroViaSecundaria(String numeroViaSecundaria) {
        this.numeroViaSecundaria = numeroViaSecundaria;
    }

    public String getLetrasViaSecundaria() {
        return letrasViaSecundaria;
    }

    public void setLetrasViaSecundaria(String letrasViaSecundaria) {
        this.letrasViaSecundaria = letrasViaSecundaria;
    }

    public boolean isBisViaSecundaria() {
        return bisViaSecundaria;
    }

    public void setBisViaSecundaria(boolean bisViaSecundaria) {
        this.bisViaSecundaria = bisViaSecundaria;
    }

    public String getNumeroViaComplementaria() {
        return numeroViaComplementaria;
    }

    public void setNumeroViaComplementaria(String numeroViaComplementaria) {
        this.numeroViaComplementaria = numeroViaComplementaria;
    }

    public String getCardinalViaComplementaria() {
        return cardinalViaComplementaria;
    }

    public void setCardinalViaComplementaria(String cardinalViaComplementaria) {
        this.cardinalViaComplementaria = cardinalViaComplementaria;
    }

    public Map<String, String> getComplementos() {
        return complementos;
    }

    public void setComplementos(Map<String, String> complementos) {
        this.complementos = complementos;
    }

    public String getDireccionCompleta() {
        return direccionCompleta;
    }

    public void setDireccionCompleta(String direccionCompleta) {
        this.direccionCompleta = direccionCompleta;
    }
    //</editor-fold>
}
