package edu.iasp.pruebas.web.componente01.exceptions.enums;

public enum CodigoMensajeErrorEnum {
    CALLE_NO_SELECCIONADA("006", "msg_direccion_error_seleccione_una_calle"),
    VIA_PRINCIPAL_NO_DIGITADA("007", "msg_direccion_error_seleccione_una_via_principal");

    private String codigo;
    private String mensajeI18n;

    CodigoMensajeErrorEnum(String codigo, String mensajeI18n) {
        this.codigo = codigo;
        this.mensajeI18n = mensajeI18n;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getMensajeI18n() {
        return mensajeI18n;
    }
}
