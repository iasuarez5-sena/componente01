package edu.iasp.pruebas.web.componente01.utils;

import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class ViewUtils {
    
    private ViewUtils(){}


    public static void closeDialog(String dialogWV) {
        RequestContext.getCurrentInstance().getScriptsToExecute().add("PF('" + dialogWV +"').hide();");
    }

    public static void addErrorMessage(String message, String detail) {
        addMessage(null, message, detail, FacesMessage.SEVERITY_ERROR);
    }
    public static void addErrorMessage(String clientId, String message, String detail) {
        addMessage(clientId, message, detail, FacesMessage.SEVERITY_ERROR);
    }

    private static void addMessage(String clientId, String message, String detail, FacesMessage.Severity severity){
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(severity, message, detail));
    }
}
