package edu.iasp.pruebas.web.componente01.exceptions.direcciones;

import edu.iasp.pruebas.web.componente01.exceptions.enums.CodigoMensajeErrorEnum;

public class DireccionInvalidaException extends Exception{

	private CodigoMensajeErrorEnum codigo;
	/**
	 * 
	 */
	private static final long serialVersionUID = 3177597189797870057L;

	public DireccionInvalidaException(CodigoMensajeErrorEnum codigo) {
		super(codigo.name());
		this.codigo = codigo;
	}

	public CodigoMensajeErrorEnum getCodigo() {
		return codigo;
	}

	public void setCodigo(CodigoMensajeErrorEnum codigo) {
		this.codigo = codigo;
	}
}
