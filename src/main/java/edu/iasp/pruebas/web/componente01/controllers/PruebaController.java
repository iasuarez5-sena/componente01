package edu.iasp.pruebas.web.componente01.controllers;

import org.slf4j.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class PruebaController implements Serializable {

    @Inject
    private Logger log;

    private String direccion1;
    private String direccion2;
    private String direccion3;

    public void action() {
        showInLog("dirección 1", direccion1);
        showInLog("dirección 2", direccion2);
        showInLog("dirección 3", direccion3);
    }

    private void showInLog(String label, String valor) {
        log.info("====================================================================================================");
        log.info("**** El valor de " + label + " es : " + valor);
        log.info("====================================================================================================");
    }

    //<editor-fold  defaultstate="collapsed" desc="Getters && Setters">
    public String getDireccion1() {
        return direccion1;
    }

    public void setDireccion1(String direccion1) {
        this.direccion1 = direccion1;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getDireccion3() {
        return direccion3;
    }

    public void setDireccion3(String direccion3) {
        this.direccion3 = direccion3;
    }
    //</editor-fold>
}
