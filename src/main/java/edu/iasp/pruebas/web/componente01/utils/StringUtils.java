package edu.iasp.pruebas.web.componente01.utils;

import java.util.Objects;

public class StringUtils {

    private StringUtils(){}

    public static boolean isEmpty(String value) {
        return Objects.isNull(value) || value.isEmpty();
    }

    public static boolean isEmptyWithTrim(String value) {
        return Objects.isNull(value) || value.trim().isEmpty();
    }
}
