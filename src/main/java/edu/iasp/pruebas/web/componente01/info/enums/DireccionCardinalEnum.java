package edu.iasp.pruebas.web.componente01.info.enums;

import java.io.Serializable;

public enum DireccionCardinalEnum implements Serializable {
    ESTE, NORTE, OESTE, SUR
}
